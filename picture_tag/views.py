import json

from django.contrib.auth.models import User
from django.db.models import Q
from django.shortcuts import render, HttpResponse

from .picture_tag.models import Tag


def add(request):

    tag = Tag()
    
    tag.image_key = request.GET.get('image_id')
    tag.text = request.GET.get('name')
    tag.left = request.GET.get('left')
    tag.top = request.GET.get('top')
    tag.width = request.GET.get('width')
    tag.height = request.GET.get('height')

    tag.save()

    return HttpResponse( json.dumps({
        'result': tag.id != None,
        'tag': tag.getData(),
    }) )


def autoComplete(request):

    users = []
    q = request.GET.get('term')

    result = User.objects.filter(Q(first_name__startswith=q) | Q(last_name__startswith=q))[:5]

    for user in result:
        users.append({
            'id': user.id,
            'label': user.first_name + ' ' + user.last_name,
            'value': user.first_name + ' ' + user.last_name,
        })

    return HttpResponse( json.dumps(users) )


def remove(request):

    Tag.objects.filter(
        id=request.GET.get('tag-id'),
        image_key=request.GET.get('id')
    ).delete()

    return HttpResponse( json.dumps({
        'result': True,
        'message': 'ooops'
    }) )


def list(request):

    tagList = []

    for tag in Tag.objects.filter(image_key= request.GET.get('id') ):
        tagList.append( tag.getData() )

    data = {
        'Image' : [{
            'id':request.GET.get('id'),
            'Tags':tagList
        }],
        # 'options': {
        #     'literals': {
        #         'removeTag': 'Remove tag'
        #     },
        #     'tag':{
        #        'flashAfterCreation': True
        #     }
        # }
    }

    return HttpResponse( json.dumps(data) )


def test(request):
    return render(request, 'templates/picture_tag/view.html')