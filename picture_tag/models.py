from django.db import models


class Tag(models.Model):

    image_key = models.CharField(max_length=200)
    text = models.CharField(max_length=200)
    left = models.FloatField(default=0)
    top = models.FloatField(default=0)
    width  = models.FloatField(default=120)
    height  = models.FloatField(default=120)


    def getData(self):
        return {
            'id': self.id,
            'text': self.text,
            'left': self.left,
            'top': self.top,
            'width': self.width,
            'height': self.height,
            # 'url': 'person.php?id=',
            'isDeleteEnable': True,
        }