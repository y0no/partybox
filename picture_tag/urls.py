# -*- coding: utf-8 -*-

from django.conf.urls import patterns, url

from .picture_tag.views import *

urlpatterns = patterns('',
    url(r'^$', test),
    url(r'^add$', add, name='picture-tag-add'),
    url(r'^auto-complete$', autoComplete, name='picture-tag-auto-complete'),
    url(r'^del$', remove, name='picture-tag-del'),
    url(r'^list$', list, name='picture-tag-list'),
)