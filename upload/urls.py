#! /usr/bin/env python
# -*- coding: utf-8 -*-
# vim:fenc=utf-8
# vim: ai ts=4 sts=4 et sw=4 nu
#
# Copyright © 2013 y0no yoann.onoditbiot@gmail.com
#


from django.conf.urls import patterns, url

from upload.views import *

urlpatterns = patterns('',
    url(r'^$', form, name='index'),
    url(r'^process/(?P<box_id>\S+)', upload, name='process'),
)
