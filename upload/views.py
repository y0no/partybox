import json
import hashlib
import random
import traceback

from django.http import HttpResponse
from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.core.exceptions import PermissionDenied, SuspiciousOperation
from django.db.models import Q

from boxes.models import Box, Picture


def form(request):
    return render(request, 'templates/upload/index.html')


@login_required
def upload(request, box_id):
    if request.method != "POST":
        raise SuspiciousOperation

    response = {}
    response['file'] = {}

    if not Box.objects.filter(Q(owner=request.user)
                              | Q(participants=request.user)).exists():
        raise PermissionDenied

    for picture in request.FILES.getlist('file'):
        hashedName = hashlib.sha1(str(random.random()).encode('utf-8')).hexdigest()
        extension = picture.name.split(".")[-1]
        picture.name = "{0}.{1}".format(hashedName, extension)

        try:
            Picture.objects.create(
                box_id=box_id,
                owner=request.user,
                picture=picture,
            )

            response['file'][picture.name] = 'OK'
        except Exception as e:
            print e
            print traceback.format_exc()
            response['file'][picture.name] = str(e)

    return HttpResponse(json.dumps(response), content_type="application/json")
