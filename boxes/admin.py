from django.contrib import admin

from .models import Box, Picture


admin.site.register(Box)
admin.site.register(Picture)