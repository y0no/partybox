# Create your views here.
import hashlib
import random
import json

from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse_lazy
from django.http import HttpResponse, HttpResponseRedirect
from django.utils.decorators import method_decorator
from django.templatetags.static import static
from django.views.generic import ListView, CreateView, UpdateView, \
                                 DeleteView, DetailView, TemplateView

from .models import *
from .forms import *

class JSONResponseMixin(object):
    """
    A mixin that can be used to render a JSON response.
    """
    def render_to_json_response(self, object, **response_kwargs):
        """
        Returns a JSON response, transforming 'context' to make the payload.
        """
        if 'values' in response_kwargs:
            values = response_kwargs['values']
            response_kwargs.pop('values')
        else:
            values = None

        return HttpResponse(
            self.convert_context_to_json(object, values),
            content_type='application/json',
            **response_kwargs
        )

    def convert_context_to_json(self, object, values):
        return json.dumps(list(object))


class UserLoggedMixin(object):
    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(UserLoggedMixin, self).dispatch(request, *args, **kwargs)


class BoxList(UserLoggedMixin, TemplateView):
    template_name = 'boxes/box_list.html'


class BoxListJson(JSONResponseMixin, ListView):
    model = Box

    def render_to_response(self, context, **response_kwargs):
        qs = self.get_queryset()
        result =  [{
            'name': box.name,
            'slug': box.slug,
            'pic': box.preview_pic.picture.url if box.preview_pic
                                               else static('img/Partybox.png')
           } for box in qs]

        return self.render_to_json_response(result, **response_kwargs)


class PopularBox(BoxListJson):
    model = Box

    def get_queryset(self):
        qs = super(PopularBox, self).get_queryset()
        return qs.filter(rights=0)[:5]


class UserBox(UserLoggedMixin, BoxListJson):

    def get_queryset(self):
        qs = super(UserBox, self).get_queryset()
        return qs.filter(Q(owner=self.request.user) |
                         Q(participants=self.request.user))[:5]


class SearchBox(JSONResponseMixin, ListView):

    def get_queryset(self):
        q = self.request.GET['term']
        return Box.objects.filter(Q(name__icontains=q) |
                                  Q(description__icontains=q))

    def render_to_response(self, context, **response_kwargs):
        boxes = self.get_queryset()\
                    .extra(select={'value': 'name', 'url': 'slug'})\
                    .values('value', 'url')[:5]

        data = list(boxes)

        return HttpResponse(json.dumps(data), content_type='application/json',
                            **response_kwargs)


# TODO: Picture export
class BoxView(UserLoggedMixin, DetailView):
    model = Box

    def get_queryset(self):
        return super(BoxView, self).get_queryset().select_related()


# TODO: Display initial in forms
class BoxCreate(UserLoggedMixin, CreateView):
    model = Box
    form_class = BoxForm
    fields = [
        'name',
        'location_address',
        'date_begin',
        'date_end',
        'description',
        'rights',
        'location_lat',
        'location_long'
    ]

    def get_form_kwargs(self):
        form_kwargs = super(BoxCreate, self).get_form_kwargs()
        instance = Box(owner=self.request.user)
        instance.slug = hashlib.sha1(str(random.random()).encode('utf-8'))\
                                                         .hexdigest()
        form_kwargs['instance'] = instance

        return form_kwargs

    def form_valid(self, form):
        object = form.save()
        object.participants.add(object.owner)

        return super(BoxCreate, self).form_valid(form)
        

    @permalink
    def get_success_url(self):
        return 'box_detail', (self.object.slug,)


class BoxUpdate(UserLoggedMixin, UpdateView):
    model = Box
    form_class = BoxForm

    @permalink
    def get_success_url(self):
        return 'box_detail', (self.object.slug,)


class BoxDelete(UserLoggedMixin, DeleteView):
    model = Box

    def get_success_url(self):
        return reverse_lazy('box_list')


def AddParticipant(request, slug):
    box = Box.objects.filter(slug=slug).first()
    box.participants.add(request.user)

    return HttpResponseRedirect(reverse_lazy('box_detail', args=(slug,)))

def DelParticipant(request, slug):
    box = Box.objects.filter(slug=slug).first()
    box.participants.remove(request.user)

    return HttpResponseRedirect(reverse_lazy('box_detail', args=(slug,)))