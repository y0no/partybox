# coding: utf-8

from django.db.models import *
from django.utils.encoding import python_2_unicode_compatible
from easy_thumbnails.fields import ThumbnailerImageField


@python_2_unicode_compatible
class Box(Model):
    name = CharField("Event name", max_length=200)
    owner = ForeignKey(settings.AUTH_USER_MODEL, related_name="my_boxes", verbose_name='Owner')
    slug = CharField(max_length=40)
    location_long = FloatField()
    location_lat = FloatField()
    location_address = CharField(max_length=140)
    date_begin = DateTimeField()
    date_end = DateTimeField()
    description = TextField()
    RIGHTS = enumerate((
        'public',
        'private with invitation',
        'private with URL',
    ))
    rights = PositiveSmallIntegerField(choices=RIGHTS)
    participants = ManyToManyField(settings.AUTH_USER_MODEL, related_name="boxes")

    def __str__(self):
        return self.name

    @permalink
    def get_absolute_url(self):
        return 'box_detail', (self.pk,)

    @property
    def num_participants(self):
        return self.participants.count()

    @property
    def preview_pic(self):
        return self.pictures.order_by('?').first()

    def was_here(self, user):
        return self.participants.filter(pk=user.pk).exists()

class Picture(Model):
    box = ForeignKey(Box, related_name="pictures")
    owner = ForeignKey(settings.AUTH_USER_MODEL, related_name="pictures")
    picture = ThumbnailerImageField(upload_to='pictures/')

    def __str__(self):
        return "{0} by {1}".format(self.picture.name, self.owner.username)

