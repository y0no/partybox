#coding: utf-8

try:
    from django.conf.urls import *
except ImportError:  # django < 1.4
    from django.conf.urls.defaults import *

from .views import *

# place app url patterns here
urlpatterns = patterns('',
    url(r'^$', BoxList.as_view(), name='box_list'),

    url(r'^popular_boxes$', PopularBox.as_view(), name='popular_boxes'),
    url(r'^my_boxes$', UserBox.as_view(), name='my_boxes'),
    url(r'^search$', SearchBox.as_view(), name='search'),

    url(r'^new$', BoxCreate.as_view(), name='box_new'),
    url(r'^(?P<slug>\S{40})$', BoxView.as_view(), name='box_detail'),
    url(r'^(?P<slug>\S{40})/update$', BoxUpdate.as_view(), name='box_update'),
    url(r'^(?P<slug>\S{40})/delete$', BoxDelete.as_view(), name='box_delete'),

    url(r'^(?P<slug>\S{40})/addp$', AddParticipant, name='participant_add'),
    url(r'^(?P<slug>\S{40})/delp$', DelParticipant, name='participant_del'),
)