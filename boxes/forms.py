from django import forms

from .models import Box


class BoxForm(forms.ModelForm):


    RIGHTS = (
        (0, 'public'),
        (1, 'private with invitation'),
        (2, 'private with URL'),
    )

    location_long = forms.FloatField(widget=forms.HiddenInput)
    location_lat  = forms.FloatField(widget=forms.HiddenInput)
    location_address = forms.CharField(label="Address")
    description = forms.CharField(label="Description", required=False)
    date_begin = forms.DateTimeField(initial="12/12/2013 08:08")
    date_end = forms.DateTimeField(initial="13/12/2013 08:08")
    rights = forms.ChoiceField(
                    widget=forms.RadioSelect,
                    choices=RIGHTS,
                    initial=0,
            )

    class Meta:
        model = Box
        fields = [
            'name',
            'location_address',
            'date_begin',
            'date_end',
            'description',
            'rights',
            'location_lat',
            'location_long'

        ]

    def __init__(self, *args, **kwargs):
        super(BoxForm, self).__init__(*args, **kwargs)

        self.fields['date_begin'].label     = "Begin date"
        self.fields['date_end'].label       = "End date"
        self.fields['rights'].label         = "Box rights"

