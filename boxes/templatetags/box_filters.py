__author__ = 'y0no'

from django import template

register = template.Library()

@register.filter
def was_here(obj, user):
    return obj.was_here(user)