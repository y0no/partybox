from django.conf import settings
from django.conf.urls import patterns, include, url
from django.conf.urls.static import static
from django.contrib import admin

from boxes.views import BoxList

admin.autodiscover()

urlpatterns = patterns('',
    url(r'^$', BoxList.as_view(), name='home'),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^box/', include('boxes.urls')),
    url(r'^upload/', include('upload.urls')),
    url(r'^accounts/', include('allauth.urls')),

)

if settings.DEBUG:
    import debug_toolbar
    urlpatterns += patterns('',
        url(r'^__debug__/', include(debug_toolbar.urls)),
    ) + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)